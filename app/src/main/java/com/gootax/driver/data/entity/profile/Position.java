package com.gootax.driver.data.entity.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Position {
    @SerializedName("position_id")
    @Expose
    private String positionId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("has_car")
    @Expose
    private boolean hasCar;

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasCar() {
        return hasCar;
    }

    public void setHasCar(boolean hasCar) {
        this.hasCar = hasCar;
    }
}
