package com.gootax.driver.data.network;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class GootaxInterceptor implements Interceptor {

    private Context context;

    public GootaxInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        PackageManager manager = context.getPackageManager();
        String version = "0";
        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            version = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Request request = chain.request();
        request = request.newBuilder()
                .addHeader("User-Agent", "driver/" + version + " (" + "android " + Build.VERSION.RELEASE + "; " + Build.MANUFACTURER + " " + Build.MODEL + ")")
                .addHeader("Connection", "Keep-Alive")
                .build();

        Response response = chain.proceed(request);
        return response;
    }
}
