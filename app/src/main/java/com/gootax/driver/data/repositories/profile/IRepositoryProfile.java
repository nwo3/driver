package com.gootax.driver.data.repositories.profile;

import com.gootax.driver.data.entity.profile.City;
import com.gootax.driver.data.entity.profile.Position;
import com.gootax.driver.data.entity.profile.Profile;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;

public interface IRepositoryProfile {
    void add();
    void delete(Profile profile);
    Profile getProfileById(long id);
    Single<List<City>> loadCityList();
    Single<List<Position>> loadPositionList();
    Single<Profile> loadProfile();
}
