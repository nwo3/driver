package com.gootax.driver.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceRequest {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("result")
    @Expose
    private ServiceInfo result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ServiceInfo getResult() {
        return result;
    }

    public void setResult(ServiceInfo result) {
        this.result = result;
    }

}
