package com.gootax.driver.data.entity;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Table(name = "ServiceInfo")
public class ServiceInfo extends Model {
    @SerializedName("auth_result")
    @Expose
    private int authResult;
    @Column(name = "workerSecretKey")
    @SerializedName("worker_secret_key")
    @Expose
    private String workerSecretKey;
    @Column(name = "companyName")
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @Column(name = "companyLogo")
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @Column(name = "domen")
    private String domen;
    @Column(name = "driverSign")
    private String driverSign;
    @Column(name = "password")
    private String password;
    @Column(name = "comment")
    private String comment;

    public int getAuthResult() {
        return authResult;
    }

    public void setAuthResult(int authResult) {
        this.authResult = authResult;
    }

    public String getWorkerSecretKey() {
        return workerSecretKey;
    }

    public void setWorkerSecretKey(String workerSecretKey) {
        this.workerSecretKey = workerSecretKey;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getDomen() {
        return domen;
    }

    public void setDomen(String domen) {
        this.domen = domen;
    }

    public String getDriverSign() {
        return driverSign;
    }

    public void setDriverSign(String driverSign) {
        this.driverSign = driverSign;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
