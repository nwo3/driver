package com.gootax.driver.data.gcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gootax.driver.R;
import com.gootax.driver.data.AppParams;
import com.gootax.driver.data.AppPreferences;
import com.gootax.driver.screen.splashscreen.SplashActivity;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

public class MyGcmListenerService extends GcmListenerService {

    public static final int NOTIFICATION_ID = 1;
    public static final int NOTIFICATION_ID_OFFER = 4;
    private NotificationManager mNotificationManager;
    Intent intentWorkDay, intentWorkDayOrdersFree, intentWorkDayOrdersPree, intentWorkDayHelp, intentWorkDayOrder, intentStatusOrder;
    AppPreferences appPreferences;



    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param extras Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle extras) {

        try {
            Log.d("PUSH_LISTENER", String.valueOf(extras));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String command = extras.getString("command");



        if (from.equals(AppParams.SENDER_ID) && command != null) {

            appPreferences = new AppPreferences(getApplicationContext());
            JSONObject jsonObjectHelp = null;

            switch (command) {
                case "update_free_orders":
                    sendBroadcast(intentWorkDayOrdersFree);

                    try {
                        if (extras.getString("sound").equals("new_free.aiff")) {
                            if (appPreferences.getText("in_shift").equals("1") || appPreferences.getText("push_show_freeorder").equals("1")) {
                                //if (appPreferences.getText("push_sound_freeorder").equals("1")) sendNotificationUpdate(command, true);
                                //else sendNotificationUpdate(command, false);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "update_pre_orders":
                    sendBroadcast(intentWorkDayOrdersPree);

                    try {
                        if (extras.getString("sound").equals("new_free.aiff")) {
                            if (appPreferences.getText("in_shift").equals("1") || appPreferences.getText("push_show_preorder").equals("1")) {
                                //if (appPreferences.getText("push_sound_preorder").equals("1")) sendNotificationUpdate(command, true);
                                //else sendNotificationUpdate(command, false);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "new_balance":
                    JSONObject jsonObjectBalance = null;
                    try {
                        jsonObjectBalance = new JSONObject(extras.getString("command_params"));


                    } catch (JSONException | NullPointerException | NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "sos":
                    try {
                        jsonObjectHelp = new JSONObject(extras.getString("command_params"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "unsos":
                    try {
                        jsonObjectHelp = new JSONObject(extras.getString("command_params"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "remind_pre_order":

                    break;
                case "new_message":

                    break;
                case "bulk_notify":

                    break;
                case "remove_from_reserved_pre_order":

                    sendNotification(extras.getString("tickerText"), extras.getString("message"), true, command);
                    break;
                case "remove_from_reserved_order":

                    sendNotification(extras.getString("tickerText"), extras.getString("message"), true, command);
                    break;
                case "assign_at_pre_order":

                    sendNotification(extras.getString("tickerText"), extras.getString("message"), true, command);
                    break;
                case "assign_at_order":

                    sendNotification(extras.getString("tickerText"), extras.getString("message"), true, command);
                    break;
            }
        }

    }


    private void sendNotification(String title, String msg, boolean sound, String command) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.push)
                        .setContentTitle(title)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        if (sound) {
            switch (command) {
                case "order_is_rejected":
                    //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.rejected));
                    break;
                case "update_free_orders":

                    break;
                case "remind_pre_order":
                    //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.remind_preorder));
                    break;
                case "update_pre_orders":

                    break;
                case "assign_at_pre_order":
                    //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.own));
                    break;
                case "assign_at_order":
                    //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.own));
                    break;
                default:
                    //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.marimba_chord));

            }
        }


        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void sendNotificationBulk(String title, String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)

                        .setSmallIcon(R.drawable.push)
                        .setContentTitle(title)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder.setContentIntent(contentIntent);
        //mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.marimba_chord));

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}

