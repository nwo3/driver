package com.gootax.driver.data;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

    private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;

    public AppPreferences(Context context) {
        this.sharedPreferences = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPreferences.edit();
    }

    public String getText(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void saveText(String key, String text) {
        prefsEditor.putString(key, text);
        prefsEditor.commit();
    }

    public void clear() {
        prefsEditor.clear();
        prefsEditor.commit();
    }

}
