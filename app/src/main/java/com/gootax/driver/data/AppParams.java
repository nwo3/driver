package com.gootax.driver.data;


import com.gootax.driver.R;

public class AppParams {

    public static final String URL = "https://da.uatgootax.ru/v3/api/";
    //public static final String URL = "https://da2.gootax.pro:8088/v3/api";
    public static final String SOCKET = "https://ch.uatgootax.ru";
    //public static final String SOCKET = "https://ch2.gootax.pro:3003";
    public static final String URL_GEOCODE = "https://geo.gootax.pro/v1/autocomplete";

    /** Theme default */
    public static final int THEME_DEFAULT = R.style.AppThemeLight;
    public static final int[] THEMES = {R.style.AppThemeLight, R.style.AppThemeDark};

    public static final String SENDER_ID = "154609468944";
}
