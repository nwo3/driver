package com.gootax.driver.data.repositories.profile;


import com.activeandroid.query.Select;
import com.gootax.driver.data.entity.profile.City;
import com.gootax.driver.data.entity.profile.Position;
import com.gootax.driver.data.entity.profile.Profile;
import com.gootax.driver.data.network.GootaxApi;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RepositoryProfile implements IRepositoryProfile {

    GootaxApi gootaxApi;

    @Inject
    public RepositoryProfile(GootaxApi gootaxApi) {
        this.gootaxApi = gootaxApi;
    }

    @Override
    public void add() {

    }

    @Override
    public void delete(Profile profile) {
        profile.delete();
    }

    @Override
    public Profile getProfileById(long id) {
        return new Select().from(Profile.class).where("Id = ?", id).executeSingle();
    }

    @Override
    public Single<List<City>> loadCityList() {
        return null;
    }

    @Override
    public Single<List<Position>> loadPositionList() {
        return null;
    }

    @Override
    public Single<Profile> loadProfile() {
        return null;
    }

}
