package com.gootax.driver.data.entity;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Orders")
public class Order extends Model {

    @Column(name = "orderId")
    private String orderId;

    //@Column(name = "profile", onDelete = Column.ForeignKeyAction.CASCADE)
    //private Profile profile;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
