package com.gootax.driver.data.entity.profile;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.List;

@Table(name = "Profiles")
public class Profile extends Model {
    @Column(name = "allow_work_without_gps")
    private int allowWorkWithoutGps = 0;
    @Column(name = "allow_refill_balance_by_bank_card")
    private int allowRefillBalanceByBankCard = 0;
    @Column(name = "allow_cancel_order")
    private int allowCancelOrder = 0;
    @Column(name = "allow_cancel_order_after_time")
    private int allowCancelOrderAfterTime = 0;
    @Column(name = "show_chat_with_dispatcher_before_start_shift")
    private int showChatWithDispatcherBeforeStartShift = 0;
    @Column(name = "show_chat_with_dispatcher_on_shift")
    private int showChatWithDispatcherOnShift = 0;
    @Column(name = "show_general_chat_on_shift")
    private int showGeneralChatOnShift = 0;
    @Column(name = "start_time_by_order")
    private int startTimeByOrder = 0;
    @Column(name = "show_estimation")
    private int showEstimation = 0;
    @Column(name = "allow_edit_order")
    private int allowEditOrder = 0;
    @Column(name = "show_urgent_order_time")
    private int showUrgentOrderTime = 0;
    @Column(name = "show_worker_address")
    private int showWorkerAddress = 0;

    @Column(name = "callsign")
    private String callsign;

    @Column(name = "exp")
    private String exp;

    @Column(name = "driverName")
    private String driverName;

    @Column(name = "serviceName")
    private String serviceName;

    @Column(name = "secretCode")
    private String secretCode;

    public Profile() {
        super();
    }

    public int getAllowWorkWithoutGps() {
        return allowWorkWithoutGps;
    }

    public void setAllowWorkWithoutGps(int allowWorkWithoutGps) {
        this.allowWorkWithoutGps = allowWorkWithoutGps;
    }

    public int getAllowRefillBalanceByBankCard() {
        return allowRefillBalanceByBankCard;
    }

    public void setAllowRefillBalanceByBankCard(int allowRefillBalanceByBankCard) {
        this.allowRefillBalanceByBankCard = allowRefillBalanceByBankCard;
    }

    public int getAllowCancelOrder() {
        return allowCancelOrder;
    }

    public void setAllowCancelOrder(int allowCancelOrder) {
        this.allowCancelOrder = allowCancelOrder;
    }

    public int getAllowCancelOrderAfterTime() {
        return allowCancelOrderAfterTime;
    }

    public void setAllowCancelOrderAfterTime(int allowCancelOrderAfterTime) {
        this.allowCancelOrderAfterTime = allowCancelOrderAfterTime;
    }

    public int getShowChatWithDispatcherBeforeStartShift() {
        return showChatWithDispatcherBeforeStartShift;
    }

    public void setShowChatWithDispatcherBeforeStartShift(int showChatWithDispatcherBeforeStartShift) {
        this.showChatWithDispatcherBeforeStartShift = showChatWithDispatcherBeforeStartShift;
    }

    public int getShowChatWithDispatcherOnShift() {
        return showChatWithDispatcherOnShift;
    }

    public void setShowChatWithDispatcherOnShift(int showChatWithDispatcherOnShift) {
        this.showChatWithDispatcherOnShift = showChatWithDispatcherOnShift;
    }

    public int getShowGeneralChatOnShift() {
        return showGeneralChatOnShift;
    }

    public void setShowGeneralChatOnShift(int showGeneralChatOnShift) {
        this.showGeneralChatOnShift = showGeneralChatOnShift;
    }

    public int getStartTimeByOrder() {
        return startTimeByOrder;
    }

    public void setStartTimeByOrder(int startTimeByOrder) {
        this.startTimeByOrder = startTimeByOrder;
    }

    public int getShowEstimation() {
        return showEstimation;
    }

    public void setShowEstimation(int showEstimation) {
        this.showEstimation = showEstimation;
    }

    public int getAllowEditOrder() {
        return allowEditOrder;
    }

    public void setAllowEditOrder(int allowEditOrder) {
        this.allowEditOrder = allowEditOrder;
    }

    public int getShowUrgentOrderTime() {
        return showUrgentOrderTime;
    }

    public void setShowUrgentOrderTime(int showUrgentOrderTime) {
        this.showUrgentOrderTime = showUrgentOrderTime;
    }

    public int getShowWorkerAddress() {
        return showWorkerAddress;
    }

    public void setShowWorkerAddress(int showWorkerAddress) {
        this.showWorkerAddress = showWorkerAddress;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }
}
