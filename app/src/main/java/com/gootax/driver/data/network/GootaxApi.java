package com.gootax.driver.data.network;


import com.gootax.driver.data.entity.BalanceRequest;
import com.gootax.driver.data.entity.ServiceRequest;
import com.gootax.driver.data.entity.profile.CityRequest;
import com.gootax.driver.data.entity.profile.PositionRequest;
import com.gootax.driver.data.entity.profile.ProfileRequest;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;

public interface GootaxApi {
    @GET("auth_worker")
    Single<ServiceRequest> authWorker(
            @QueryMap Map<String, String> params);

    @GET("get_worker_cities")
    Single<CityRequest> getWorkerCities(
            @Header("signature") String signature,
            @Header("worker-device") String workerDevice,
            @Header("lang") String lang,
            @Header("User-Agent") String userAgent,
            @Header("Connection") String connection,
            @Header("version") String version,
            @QueryMap Map<String, String> params);

    @GET("get_worker_positions")
    Single<PositionRequest> getWorkerPositions(
            @Header("signature") String signature,
            @Header("worker-device") String workerDevice,
            @Header("lang") String lang,
            @Header("User-Agent") String userAgent,
            @Header("Connection") String connection,
            @Header("version") String version,
            @QueryMap Map<String, String> params);

    @GET("get_worker_profile")
    Single<ProfileRequest> getWorkerProfile(
            @Header("signature") String signature,
            @Header("worker-device") String workerDevice,
            @Header("lang") String lang,
            @Header("User-Agent") String userAgent,
            @Header("Connection") String connection,
            @Header("version") String version,
            @QueryMap Map<String, String> params);

    @GET("get_worker_balance")
    Single<BalanceRequest> getWorkerBalance(
            @Header("signature") String signature,
            @Header("worker-device") String workerDevice,
            @Header("lang") String lang,
            @Header("User-Agent") String userAgent,
            @Header("Connection") String connection,
            @Header("version") String version,
            @QueryMap Map<String, String> params);
}
