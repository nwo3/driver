package com.gootax.driver.data.repositories.taxiservice;


import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.data.entity.ServiceRequest;

import java.util.List;

import io.reactivex.Single;

public interface IRepositoryTaxiService {
    Single<ServiceRequest> addTaxiService(String deviceToken, String tenantLogin, String workerLogin, String workerPassword);
    List<ServiceInfo> getListTaxiServices();
    ServiceInfo getServiceInfo(long id);
    void deleteTaxiService(long id);
}
