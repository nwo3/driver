package com.gootax.driver.data.entity.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PositionRequest {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("result")
    @Expose
    private PositionResult result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public PositionResult getResult() {
        return result;
    }

    public void setResult(PositionResult result) {
        this.result = result;
    }
}
