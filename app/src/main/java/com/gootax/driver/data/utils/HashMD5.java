package com.gootax.driver.data.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class HashMD5 {

    public static String getHash(String str) {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(str.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32)
                hashtext = "0" + hashtext;


            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static String getSignature(Map<String,String> params, String key) {
        String rawSignature = params.toString();

        String signature = rawSignature.substring(1, rawSignature.length() - 1)
                .replaceAll(", ", "&")
                .replaceAll("%5C%2F", "%2F%2F")
                .replaceAll(":", "%3A")
                .replaceAll("\\+", "%20")
                .replaceAll("\\*", "%2A")
                .replaceAll("\\|", "%7C")
                + key;

        return HashMD5.getHash(signature);
    }
}
