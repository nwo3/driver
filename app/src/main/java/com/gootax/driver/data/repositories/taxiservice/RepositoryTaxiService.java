package com.gootax.driver.data.repositories.taxiservice;


import com.activeandroid.query.Select;
import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.data.entity.ServiceRequest;
import com.gootax.driver.data.network.GootaxApi;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;

public class RepositoryTaxiService implements IRepositoryTaxiService {

    GootaxApi gootaxApi;

    @Inject
    public RepositoryTaxiService(GootaxApi gootaxApi) {
        this.gootaxApi = gootaxApi;
    }


    @Override
    public Single<ServiceRequest> addTaxiService(String deviceToken, String tenantLogin, String workerLogin, String workerPassword) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("device_token", deviceToken);
        params.put("tenant_login", tenantLogin);
        params.put("worker_login", workerLogin);
        params.put("worker_password", workerPassword);

        return gootaxApi.authWorker(params);
    }

    @Override
    public List<ServiceInfo> getListTaxiServices() {
        return new Select().from(ServiceInfo.class).execute();
    }

    @Override
    public ServiceInfo getServiceInfo(long id) {
        return new Select().from(ServiceInfo.class).where("Id = ?", id).executeSingle();
    }

    @Override
    public void deleteTaxiService(long id) {
        ServiceInfo serviceInfo = new Select().from(ServiceInfo.class).where("Id = ?", id).executeSingle();
        if (serviceInfo != null) serviceInfo.delete();
    }
}
