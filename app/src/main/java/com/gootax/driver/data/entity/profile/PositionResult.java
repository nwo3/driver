package com.gootax.driver.data.entity.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PositionResult {
    @SerializedName("positions")
    @Expose
    private List<Position> positions = null;

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
}
