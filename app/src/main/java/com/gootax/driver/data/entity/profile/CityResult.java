package com.gootax.driver.data.entity.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityResult {

    @SerializedName("cities")
    @Expose
    private List<City> cities = null;
    @SerializedName("last_city_id")
    @Expose
    private String lastCityId;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public String getLastCityId() {
        return lastCityId;
    }

    public void setLastCityId(String lastCityId) {
        this.lastCityId = lastCityId;
    }


}
