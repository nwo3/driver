package com.gootax.driver.screen.entrancescreen;


import android.util.Log;

import com.gootax.driver.data.entity.ServiceRequest;
import com.gootax.driver.data.repositories.taxiservice.RepositoryTaxiService;
import com.gootax.driver.screen.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class EntrancePresenter extends BasePresenter implements EntranceContract.Presenter {

    private EntranceContract.View view;

    @Inject
    public RepositoryTaxiService repositoryTaxiService;

    @Inject
    public EntrancePresenter(EntranceContract.View view) {
        this.view = view;
    }

    public void addProfile() {
        repositoryTaxiService.addTaxiService(
                "dycKN0afHDc:APA91bGvXlRtBFFThU-_AuwWUadc3YHyPvOjE3vQm5dUu3GCpbRtFtW5t0aHtYdHIdvvd8Ggpv7xzLSais1vyDMEaqFSdU1w69fY-Fvl1bMDsYa0lAfDkYxUJRwQs0PZkiOc9JQiAweU",
                "3colors",
                "8",
                "123123")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<ServiceRequest>() {
            @Override
            public void onSuccess(ServiceRequest serviceRequest) {
                Log.d("RETROFIT", "onNext");
                Log.d("RETROFIT", serviceRequest.getInfo());
                Log.d("RETROFIT", serviceRequest.getResult().getWorkerSecretKey());
                serviceRequest.getResult().save();
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public void startWorkShift() {

    }

    public void deleteProfile(long id) {

    }

    public void getProfileList() {
        /*
        Single.fromCallable(() -> repositoryProfile.getAllProfiles())
                .subscribe(new DisposableSingleObserver<List<Profile>>() {
                    @Override
                    public void onSuccess(List<Profile> profiles) {
                        view.showProfileList(profiles);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showEmptyList();
                    }
                });
                */

    }
}
