package com.gootax.driver.screen.entrancescreen.entranceregistration;

import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class EntranceRegistrationModule {
    private final EntranceRegistrationContract.IView view;

    public EntranceRegistrationModule(EntranceRegistrationContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    EntranceRegistrationContract.IView provideEntranceView() {
        return view;
    }
}
