package com.gootax.driver.screen.entrancescreen.entranceedit;

import com.gootax.driver.data.entity.ServiceInfo;

public interface EntranceEditContract {

    interface IView {
        void showServiceInfo(ServiceInfo serviceInfo);
        void emptyServiceInfo();
        void startEntranceListActivity();
    }

    interface IPresenter {
        void getServiceInfo(long serviceId);
        void updateServiceInfo(long serviceId);
        void deleteServiceInfo(long serviceId);
    }
}
