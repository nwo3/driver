package com.gootax.driver.screen.workscreen;


import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class WorkModule {
    private final WorkContract.IView view;

    public WorkModule(WorkContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    WorkContract.IView provideWorkView() {
        return view;
    }
}
