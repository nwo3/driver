package com.gootax.driver.screen.entrancescreen.entranceedit;


import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.data.repositories.taxiservice.RepositoryTaxiService;
import com.gootax.driver.screen.BasePresenter;

import javax.inject.Inject;

public class EntranceEditPresenter extends BasePresenter implements EntranceEditContract.IPresenter {

    private EntranceEditContract.IView view;

    @Inject
    public RepositoryTaxiService repositoryTaxiService;

    @Inject
    public EntranceEditPresenter(EntranceEditContract.IView view) {
        this.view = view;
    }

    @Override
    public void getServiceInfo(long serviceId) {
        ServiceInfo serviceInfo = repositoryTaxiService.getServiceInfo(serviceId);
        if (serviceInfo != null) {
            view.showServiceInfo(serviceInfo);
        } else {

        }
    }

    @Override
    public void updateServiceInfo(long serviceId) {

    }

    @Override
    public void deleteServiceInfo(long serviceId) {
        repositoryTaxiService.deleteTaxiService(serviceId);
        view.startEntranceListActivity();
    }
}
