package com.gootax.driver.screen.entrancescreen;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.gootax.driver.App;
import com.gootax.driver.R;
import com.gootax.driver.data.entity.profile.Profile;

import java.util.List;

import javax.inject.Inject;

public class EntranceActivity extends AppCompatActivity implements EntranceContract.View, EntranceAdapter.IEntranceAdapter {

    @Inject
    EntrancePresenter entrancePresenter;


    private FloatingActionButton mFab;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private List<String> mServiceInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_entrance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DaggerEntranceComponent.builder()
                .networkComponent(((App) getApplication()).getNetworkComponent())
                .entranceModule(new EntranceModule(this))
                .build().inject(this);

        initViews();

        entrancePresenter.addProfile();

        /*
        String[] adapterData = new String[]{"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"};
        mServiceInfos = new ArrayList<>(Arrays.asList(adapterData));

        mAdapter = new EntranceAdapter(this, getApplicationContext(), mServiceInfos);
        ((EntranceAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnScrollListener(onScrollListener);

        mFab.setOnClickListener(view -> {
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            entrancePresenter.addProfile();
        });

        entrancePresenter.getProfileList();
        */
    }



    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            Log.e("ListView", "onScrollStateChanged");
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // Could hide open views here if you wanted. //
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_entrance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProfileList(List<Profile> profiles) {
        for (Profile profile : profiles) {
            Log.d("ACTIVITY_ENTRANCE",  "rx d " + profile.getServiceName());
        }
    }

    @Override
    public void showEmptyList() {

    }

    private void initViews() {

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        //mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        // Layout Managers:
        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Item Decorator:
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.HORIZONTAL));
        //mRecyclerView.setItemAnimator(new FadeInLeftAnimator());
    }

    @Override
    public void onClickItem(int position) {
        Log.d("RECYCLERVIEW", "№ " + position);
    }

    @Override
    public void onDeleteItem(int position) {

    }
}
