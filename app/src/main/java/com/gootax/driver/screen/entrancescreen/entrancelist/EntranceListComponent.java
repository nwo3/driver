package com.gootax.driver.screen.entrancescreen.entrancelist;

import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {EntranceListModule.class, GootaxApiModule.class})
public interface EntranceListComponent {
    void inject(EntranceListActivity entranceListActivity);
}
