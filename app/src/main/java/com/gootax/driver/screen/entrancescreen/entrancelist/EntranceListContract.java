package com.gootax.driver.screen.entrancescreen.entrancelist;


import com.gootax.driver.data.entity.ServiceInfo;

import java.util.List;

public interface EntranceListContract {

    interface IView {
        void showListService(List<ServiceInfo> serviceInfoList);
        void startEntranceRegistrationActivity();
    }

    interface IPresenter {
        void loadListService();

    }
}
