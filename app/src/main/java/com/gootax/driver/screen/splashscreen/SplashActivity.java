package com.gootax.driver.screen.splashscreen;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.activeandroid.query.Select;
import com.gootax.driver.R;
import com.gootax.driver.data.AppPreferences;
import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.screen.entrancescreen.entrancelist.EntranceListActivity;
import com.gootax.driver.screen.entrancescreen.entranceregistration.EntranceRegistrationActivity;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private AppPreferences appPreferences;
    private String regid;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private List<ServiceInfo> serviceInfoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()) {
            finish();
            return;
        }

        appPreferences = new AppPreferences(this);

        serviceInfoList = new Select().from(ServiceInfo.class).execute();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean("sentTokenToServer", false);
                if (sentToken) {
                    try {
                        regid = intent.getExtras().getString("token");
                        appPreferences.saveText("device_token", regid);
                        Intent intentEntrance;
                        if (serviceInfoList.size() > 0) {
                            intentEntrance = new Intent(getApplicationContext(), EntranceListActivity.class);
                        } else {
                            intentEntrance = new Intent(getApplicationContext(), EntranceRegistrationActivity.class);
                        }
                        startActivity(intentEntrance);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        /*
        if (checkPlayServices() && appPreferences.getText("device_token").length() == 0) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent;
                    if (serviceInfoList.size() > 0) {
                        intent = new Intent(getApplicationContext(), EntranceListActivity.class);
                    } else {
                        intent = new Intent(getApplicationContext(), EntranceRegistrationActivity.class);
                    }

                    startActivity(intent);
                }
            }, 3000);
        }
        */

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (serviceInfoList.size() > 0) {
                    intent = new Intent(getApplicationContext(), EntranceListActivity.class);
                } else {
                    intent = new Intent(getApplicationContext(), EntranceRegistrationActivity.class);
                }

                startActivity(intent);
            }
        }, 3000);

        setContentView(R.layout.activity_splash);


    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("registrationComplete"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    /*
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }
    */
}
