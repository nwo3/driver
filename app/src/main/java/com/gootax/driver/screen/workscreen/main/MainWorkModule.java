package com.gootax.driver.screen.workscreen.main;

import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MainWorkModule {
    private final MainWorkContract.IView view;

    public MainWorkModule(MainWorkContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    MainWorkContract.IView provideMainWorkView() {
        return view;
    }
}
