package com.gootax.driver.screen.entrancescreen;


import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class EntranceModule {
    private final EntranceContract.View view;

    public EntranceModule(EntranceContract.View view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    EntranceContract.View provideEntranceView() {
        return view;
    }
}
