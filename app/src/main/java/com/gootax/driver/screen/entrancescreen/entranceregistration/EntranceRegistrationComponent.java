package com.gootax.driver.screen.entrancescreen.entranceregistration;

import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {EntranceRegistrationModule.class, GootaxApiModule.class})
public interface EntranceRegistrationComponent {
    void inject(EntranceRegistrationActivity entranceRegistrationActivity);
}
