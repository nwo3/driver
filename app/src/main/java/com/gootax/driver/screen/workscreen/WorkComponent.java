package com.gootax.driver.screen.workscreen;


import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {WorkModule.class, GootaxApiModule.class})
public interface WorkComponent {
    void inject(WorkActivity workActivity);
}
