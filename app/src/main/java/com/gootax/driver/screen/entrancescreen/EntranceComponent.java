package com.gootax.driver.screen.entrancescreen;

import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {EntranceModule.class, GootaxApiModule.class})
public interface EntranceComponent {
    void inject(EntranceActivity entranceActivity);
}
