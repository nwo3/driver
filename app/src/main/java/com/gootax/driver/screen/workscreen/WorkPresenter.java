package com.gootax.driver.screen.workscreen;


import com.gootax.driver.data.repositories.profile.RepositoryProfile;
import com.gootax.driver.screen.BasePresenter;

import javax.inject.Inject;

public class WorkPresenter extends BasePresenter implements WorkContract.IPresenter {

    private WorkContract.IView view;

    @Inject
    public RepositoryProfile repositoryProfile;

    @Inject
    public WorkPresenter(WorkContract.IView view) {
        this.view = view;
    }



}
