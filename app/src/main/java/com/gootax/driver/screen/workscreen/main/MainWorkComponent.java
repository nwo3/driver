package com.gootax.driver.screen.workscreen.main;

import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.screen.workscreen.WorkModule;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {MainWorkModule.class, GootaxApiModule.class})
public interface MainWorkComponent {
    void inject(MainWorkFragment mainWorkFragment);
}
