package com.gootax.driver.screen.entrancescreen;


import com.gootax.driver.data.entity.profile.Profile;

import java.util.List;

public interface EntranceContract {

    interface View {
        void showProfileList(List<Profile> profiles);
        void showEmptyList();
    }

    interface Presenter {

    }
}
