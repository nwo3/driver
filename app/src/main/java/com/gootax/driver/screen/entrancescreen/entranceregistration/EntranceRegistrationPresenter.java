package com.gootax.driver.screen.entrancescreen.entranceregistration;


import android.util.Log;

import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.data.entity.ServiceRequest;
import com.gootax.driver.data.repositories.taxiservice.RepositoryTaxiService;
import com.gootax.driver.screen.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class EntranceRegistrationPresenter extends BasePresenter implements EntranceRegistrationContract.IPresenter {

    private EntranceRegistrationContract.IView view;

    @Inject
    public RepositoryTaxiService repositoryTaxiService;

    @Inject
    public EntranceRegistrationPresenter(EntranceRegistrationContract.IView view) {
        this.view = view;
    }

    @Override
    public void addService(String domen, String sign, String password, String comment) {
        repositoryTaxiService.addTaxiService(
                "dycKN0afHDc:APA91bGvXlRtBFFThU-_AuwWUadc3YHyPvOjE3vQm5dUu3GCpbRtFtW5t0aHtYdHIdvvd8Ggpv7xzLSais1vyDMEaqFSdU1w69fY-Fvl1bMDsYa0lAfDkYxUJRwQs0PZkiOc9JQiAweU",
                domen,
                sign,
                password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new DisposableSingleObserver<ServiceRequest>() {
            @Override
            public void onSuccess(ServiceRequest serviceRequest) {
                Log.d("RETROFIT", serviceRequest.getInfo());
                if (serviceRequest.getInfo().equals("OK")) {
                    ServiceInfo serviceInfo = serviceRequest.getResult();
                    serviceInfo.setDomen(domen);
                    serviceInfo.setDriverSign(sign);
                    serviceInfo.setComment(comment);
                    serviceInfo.save();
                    view.startEntranceListActivity();
                } else {

                }
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
