package com.gootax.driver.screen.entrancescreen.entranceregistration;


public interface EntranceRegistrationContract {

    interface IView {
        void startEntranceListActivity();
    }

    interface IPresenter {
        void addService(String domen, String sign, String password, String comment);
    }

}
