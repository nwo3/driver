package com.gootax.driver.screen.workscreen.main;


import com.gootax.driver.data.repositories.profile.RepositoryProfile;
import com.gootax.driver.screen.BasePresenter;

import javax.inject.Inject;

public class MainWorkPresenter extends BasePresenter implements MainWorkContract.IPresenter {

    private MainWorkContract.IView view;

    @Inject
    public RepositoryProfile repositoryProfile;

    @Inject
    public MainWorkPresenter(MainWorkContract.IView view) {
        this.view = view;
    }

    public void  loadDriver() {
        repositoryProfile.loadCityList();
    }
}
