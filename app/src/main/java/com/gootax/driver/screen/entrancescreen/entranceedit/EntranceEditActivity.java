package com.gootax.driver.screen.entrancescreen.entranceedit;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gootax.driver.App;
import com.gootax.driver.R;
import com.gootax.driver.data.AppParams;
import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.screen.entrancescreen.entrancelist.EntranceListActivity;

import javax.inject.Inject;

public class EntranceEditActivity extends AppCompatActivity implements EntranceEditContract.IView {

    @Inject
    EntranceEditPresenter entranceEditPresenter;

    private EditText etDomen, etSign, etPassword, etComment;
    private TextView tvTitle;
    private long service_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerEntranceEditComponent.builder()
                .networkComponent(((App) getApplication()).getNetworkComponent())
                .entranceEditModule(new EntranceEditModule(this))
                .build().inject(this);

        setTheme(AppParams.THEME_DEFAULT);
        setContentView(R.layout.content_entrance_edit);

        initViews();

        if (getIntent().getLongExtra("service_id", -1) > 0) {
            service_id = getIntent().getLongExtra("service_id", -1);
            entranceEditPresenter.getServiceInfo(service_id);
        }
    }

    @Override
    public void showServiceInfo(ServiceInfo serviceInfo) {
        tvTitle.setText(serviceInfo.getCompanyName());
        etDomen.setText(serviceInfo.getDomen());
        etSign.setText(serviceInfo.getDriverSign());
        //etPassword.setText("");
        etComment.setText(serviceInfo.getComment());
    }

    @Override
    public void emptyServiceInfo() {

    }

    @Override
    public void startEntranceListActivity() {
        startActivity(new Intent(getApplicationContext(), EntranceListActivity.class));
    }

    public void updateService(View view) {

    }

    public void deleteService(View view) {
        if (service_id > 0)
            entranceEditPresenter.deleteServiceInfo(service_id);
    }

    private void initViews() {
        etDomen = (EditText) findViewById(R.id.edittext_host);
        etSign = (EditText) findViewById(R.id.edittext_nickname);
        etPassword = (EditText) findViewById(R.id.edittext_passdriver);
        etComment = (EditText) findViewById(R.id.edittext_commentdriver);
        tvTitle = (TextView) findViewById(R.id.tv_title_entranceedit);
    }
}
