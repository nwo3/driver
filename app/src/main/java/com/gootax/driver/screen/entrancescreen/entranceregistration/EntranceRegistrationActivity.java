package com.gootax.driver.screen.entrancescreen.entranceregistration;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.gootax.driver.App;
import com.gootax.driver.R;
import com.gootax.driver.data.AppParams;
import com.gootax.driver.screen.entrancescreen.entrancelist.EntranceListActivity;

import javax.inject.Inject;

public class EntranceRegistrationActivity extends AppCompatActivity implements EntranceRegistrationContract.IView {

    @Inject
    EntranceRegistrationPresenter entranceRegistrationPresenter;

    private EditText etDomen, etSign, etPassword, etComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerEntranceRegistrationComponent.builder()
                .networkComponent(((App) getApplication()).getNetworkComponent())
                .entranceRegistrationModule(new EntranceRegistrationModule(this))
                .build().inject(this);

        setTheme(AppParams.THEME_DEFAULT);
        setContentView(R.layout.content_entrance_registration);

        initViews();
    }

    @Override
    public void startEntranceListActivity() {
        startActivity(new Intent(this, EntranceListActivity.class));
        finish();
    }

    public void addService(View view) {
        if (etDomen.getText().toString().trim().length() > 0
                && etSign.getText().toString().trim().length() > 0
                && etPassword.getText().toString().trim().length() > 0) {
            entranceRegistrationPresenter.addService(etDomen.getText().toString(),
                    etSign.getText().toString(),
                    etPassword.getText().toString(),
                    etComment.getText().toString());
        } else {
            if (etDomen.getText().toString().trim().length() == 0) {
                etDomen.setError(getString(R.string.error_edittext_empty));
            }
            if (etSign.getText().toString().trim().length() == 0) {
                etSign.setError(getString(R.string.error_edittext_empty));
            }
            if (etPassword.getText().toString().trim().length() == 0) {
                etPassword.setError(getString(R.string.error_edittext_empty));
            }
        }
    }

    private void initViews() {
        etDomen = (EditText) findViewById(R.id.edittext_host);
        etSign = (EditText) findViewById(R.id.edittext_nickname);
        etPassword = (EditText) findViewById(R.id.edittext_passdriver);
        etComment = (EditText) findViewById(R.id.edittext_commentdriver);
    };
}
