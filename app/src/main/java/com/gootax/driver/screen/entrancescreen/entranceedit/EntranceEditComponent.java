package com.gootax.driver.screen.entrancescreen.entranceedit;

import com.gootax.driver.di.GootaxApiModule;
import com.gootax.driver.di.NetworkComponent;
import com.gootax.driver.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = NetworkComponent.class, modules = {EntranceEditModule.class, GootaxApiModule.class})
public interface EntranceEditComponent {
    void inject(EntranceEditActivity entranceEditActivity);
}
