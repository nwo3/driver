package com.gootax.driver.screen.entrancescreen.entrancelist;


import android.util.Log;

import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.data.repositories.taxiservice.RepositoryTaxiService;
import com.gootax.driver.screen.BasePresenter;

import java.util.List;

import javax.inject.Inject;

public class EntranceListPresenter extends BasePresenter implements EntranceListContract.IPresenter {

    private EntranceListContract.IView view;

    @Inject
    public RepositoryTaxiService repositoryTaxiService;

    @Inject
    public EntranceListPresenter(EntranceListContract.IView view) {
        this.view = view;
    }

    @Override
    public void loadListService() {
        List<ServiceInfo> serviceInfoList = repositoryTaxiService.getListTaxiServices();
        Log.d("TEST", " " + serviceInfoList.size());
        if (serviceInfoList.size() > 0) {
            view.showListService(serviceInfoList);
        } else {
            view.startEntranceRegistrationActivity();
        }
    }
}
