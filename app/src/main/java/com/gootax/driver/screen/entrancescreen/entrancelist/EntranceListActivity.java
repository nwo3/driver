package com.gootax.driver.screen.entrancescreen.entrancelist;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.driver.App;
import com.gootax.driver.R;
import com.gootax.driver.data.AppParams;
import com.gootax.driver.data.entity.ServiceInfo;
import com.gootax.driver.screen.entrancescreen.entranceedit.EntranceEditActivity;
import com.gootax.driver.screen.entrancescreen.entranceregistration.EntranceRegistrationActivity;
import com.gootax.driver.screen.workscreen.WorkActivity;

import java.util.List;

import javax.inject.Inject;

public class EntranceListActivity extends AppCompatActivity implements EntranceListContract.IView {

    @Inject
    EntranceListPresenter entranceListPresenter;

    private LinearLayout llContainer;
    private LayoutInflater layoutInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerEntranceListComponent.builder()
                .networkComponent(((App) getApplication()).getNetworkComponent())
                .entranceListModule(new EntranceListModule(this))
                .build().inject(this);

        setTheme(AppParams.THEME_DEFAULT);
        setContentView(R.layout.content_entrance_list);

        initViews();

        entranceListPresenter.loadListService();
    }

    @Override
    public void showListService(List<ServiceInfo> serviceInfoList) {
        llContainer.removeAllViews();
        for (ServiceInfo serviceInfo : serviceInfoList) {
            View layoutView = layoutInflater.inflate(R.layout.item_entrance_service, null, false);
            (layoutView.findViewById(R.id.fl_next)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), WorkActivity.class));
                }
            });
            (layoutView.findViewById(R.id.fl_edit)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), EntranceEditActivity.class).putExtra("service_id", serviceInfo.getId()));
                }
            });

            if (serviceInfo.getCompanyLogo() != null) {
                ((SimpleDraweeView) layoutView.findViewById(R.id.sdv_service)).setImageURI(Uri.parse(serviceInfo.getCompanyLogo()));
            }
            ((TextView) layoutView.findViewById(R.id.tv_service)).setText(serviceInfo.getCompanyName());

            llContainer.addView(layoutView);
        }
    }

    @Override
    public void startEntranceRegistrationActivity() {
        startActivity(new Intent(getApplicationContext(), EntranceRegistrationActivity.class));
    }

    public void addService(View view) {
        startActivity(new Intent(this, EntranceRegistrationActivity.class));
    }

    private void initViews() {
        llContainer = (LinearLayout) findViewById(R.id.ll_container_service);
        layoutInflater = getLayoutInflater();
    }




}
