package com.gootax.driver.screen.workscreen;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.gootax.driver.App;
import com.gootax.driver.R;
import com.gootax.driver.data.AppParams;
import com.gootax.driver.screen.workscreen.chat.ChatFragment;
import com.gootax.driver.screen.workscreen.info.InfoFragment;
import com.gootax.driver.screen.workscreen.profile.ProfileFragment;
import com.gootax.driver.screen.workscreen.settings.SettingsFragment;

import javax.inject.Inject;

public class WorkActivity extends AppCompatActivity implements WorkContract.IView {

    @Inject
    WorkPresenter workPresenter;

    private LinearLayout llMenu, llMenuSecond;
    private FrameLayout flContainer, flMainMenu1, flMainMenu2, flMainMenu3, flMainMenu4, flMainMenu5;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private Fragment fragmentProfile, fragmentSettings, fragmentInfo, fragmentChat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerWorkComponent.builder()
                .networkComponent(((App) getApplication()).getNetworkComponent())
                .workModule(new WorkModule(this))
                .build().inject(this);

        setTheme(AppParams.THEME_DEFAULT);
        setContentView(R.layout.activity_work);

        initViews();

        fragmentManager = getSupportFragmentManager();

        flContainer.post(new Runnable() {
            @Override
            public void run() {
                int height = flContainer.getHeight();
                int width = flContainer.getWidth();
                int orientation = getResources().getConfiguration().orientation;

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)flContainer.getLayoutParams();
                switch (orientation) {
                    case 1:
                        params.height = height - (width / 5);
                        params.width = width;
                        break;
                    case 2:
                        params.height = height;
                        params.width = width - (height / 5);
                        break;
                }
                flContainer.setLayoutParams(params);
            }
        });

        llMenu.post(new Runnable(){
            public void run(){
                int height = llMenu.getHeight();
                int width = llMenu.getWidth();
                int orientation = getResources().getConfiguration().orientation;

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)llMenu.getLayoutParams();
                switch (orientation) {
                    case 1:
                        params.height = width / 5;
                        params.width = width;
                        break;
                    case 2:
                        params.height = height;
                        params.width = height / 5;
                        break;
                }
                llMenu.setLayoutParams(params);
            }
        });

        llMenuSecond.post(new Runnable(){
            public void run(){
                int height = llMenuSecond.getHeight();
                int width = llMenuSecond.getWidth();
                int orientation = getResources().getConfiguration().orientation;

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)llMenuSecond.getLayoutParams();
                switch (orientation) {
                    case 1:
                        params.height = width / 4 * 2;
                        params.width = width;

                        params.setMargins(0, width / 5, 0, 0);

                        llMenuSecond.animate().translationY(-(width / 4 * 2));

                        break;
                    case 2:
                        params.height = height;
                        params.width = height / 4 * 2;

                        params.setMargins(height / 5, 0, 0, 0);

                        llMenuSecond.animate().translationX(-(height / 4 * 2));

                        break;
                }
                llMenuSecond.setLayoutParams(params);
            }
        });

        flMainMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (llMenuSecond.getVisibility() == View.INVISIBLE) llMenuSecond.setVisibility(View.VISIBLE);

                if (Integer.valueOf(llMenuSecond.getTag().toString()) == 1) {
                    hideMenuSecond(getResources().getConfiguration().orientation);
                } else {
                    showMenuSecond(getResources().getConfiguration().orientation);
                }
            }
        });

        flMainMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentProfile == null) fragmentProfile = new ProfileFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_work_containter, fragmentProfile);
                fragmentTransaction.commit();
            }
        });

        flMainMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentChat == null) fragmentChat = new ChatFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_work_containter, fragmentChat);
                fragmentTransaction.commit();
            }
        });

        flMainMenu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentInfo == null) fragmentInfo = new InfoFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_work_containter, fragmentInfo);
                fragmentTransaction.commit();
            }
        });

        flMainMenu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentSettings == null) fragmentSettings = new SettingsFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_work_containter, fragmentSettings);
                fragmentTransaction.commit();
            }
        });
    }

    private void hideMenuSecond(int orientation) {
        if (orientation == 1)
            llMenuSecond.animate().translationY(-llMenuSecond.getHeight());
        else
            llMenuSecond.animate().translationX(-llMenuSecond.getWidth());

        llMenuSecond.setTag(0);
    }

    private void showMenuSecond(int orientation) {
        if (orientation == 1)
            llMenuSecond.animate().translationY(0);
        else
            llMenuSecond.animate().translationX(0);
        llMenuSecond.setTag(1);
    }

    private void initViews() {
        llMenu = (LinearLayout) findViewById(R.id.ll_menu);
        llMenuSecond = (LinearLayout) findViewById(R.id.ll_menu_second);
        flContainer = (FrameLayout) findViewById(R.id.fl_work_containter);
        flMainMenu1 = (FrameLayout) findViewById(R.id.fl_mainmenu_1);
        flMainMenu2 = (FrameLayout) findViewById(R.id.fl_mainmenu_2);
        flMainMenu3 = (FrameLayout) findViewById(R.id.fl_mainmenu_3);
        flMainMenu4 = (FrameLayout) findViewById(R.id.fl_mainmenu_4);
        flMainMenu5 = (FrameLayout) findViewById(R.id.fl_mainmenu_5);
    }
}
