package com.gootax.driver.screen.workscreen.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gootax.driver.App;
import com.gootax.driver.R;

import javax.inject.Inject;

public class MainWorkFragment extends Fragment implements MainWorkContract.IView {

    private View view;

    @Inject
    MainWorkPresenter mainWorkPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mainwork, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DaggerMainWorkComponent.builder()
                .networkComponent(((App) getActivity().getApplication()).getNetworkComponent())
                .mainWorkModule(new MainWorkModule(this))
                .build().inject(this);


        mainWorkPresenter.loadDriver();

    }
}
