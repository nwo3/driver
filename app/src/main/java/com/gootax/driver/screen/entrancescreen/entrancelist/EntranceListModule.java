package com.gootax.driver.screen.entrancescreen.entrancelist;

import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class EntranceListModule {
    private final EntranceListContract.IView view;

    public EntranceListModule(EntranceListContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    EntranceListContract.IView provideEntranceView() {
        return view;
    }
}
