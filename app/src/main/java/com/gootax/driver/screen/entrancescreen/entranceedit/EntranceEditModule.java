package com.gootax.driver.screen.entrancescreen.entranceedit;

import com.gootax.driver.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class EntranceEditModule {
    private final EntranceEditContract.IView view;

    public EntranceEditModule(EntranceEditContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    EntranceEditContract.IView provideEntranceView() {
        return view;
    }
}
