package com.gootax.driver.di;

import com.gootax.driver.data.network.GootaxApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class GootaxApiModule {

    @Provides
    GootaxApi provideGootaxApi(Retrofit retrofit) {
        return retrofit.create(GootaxApi.class);
    }
}
