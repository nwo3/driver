package com.gootax.driver.di;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Component(modules = {AppModule.class, NetworkModule.class})
@Singleton
public interface NetworkComponent {
    Retrofit retrofit();
}
